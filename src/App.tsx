import { About } from "./components/About";
import { Footer } from "./components/Footer";
import Navbar from "./components/Navbar";
import { Projects } from "./components/Projects";
import { Skills } from "./components/Skills";
import { Welcome } from "./components/Welcome";

function App() {
  return (
    <div className="App">
      <body className="index">
        <Navbar />
        <Welcome />
        <About />
        <Skills />
        <Projects />
        <Footer />
      </body>
    </div>
  );
}

export default App;
