export interface IProject {
  id: number;
  title: string;
  description: string;
  projectLink: string;
  images: string[];
  alt?: string;
  tools?: string[];
  sourceCode?: string;
}

export const projects: IProject[] = [
  {
    id: 7,
    title: "Daily Journal",
    description: `A personal daily journal app built with Next.js and ShadCN UI, designed for jotting down thoughts, todos, and memories in Markdown while keeping everything organized and searchable. My main goal was seamless integration with my Neovim setup, allowing me to add and edit journal entries effortlessly using custom keymaps.
    I chose Next.js for this project because the content is mostly static and should be statically generated, along with it being a great opportunity to learn Next/Shadcn. Given the personal nature of my entries, I implemented authentication using NextAuth to keep them private, though I plan to add some public-facing posts over time. Also featured is a calendar heatmap to visualize my writing activity at a glance.`,
    tools: ["next", "shadcn", "tailwind", "vercel", "nextauth"],
    projectLink: "https://ericcparsons-journal.vercel.app/",
    images: ["images/journal.png"],
    alt: "journal",
  },
  {
    id: 0,
    title: "Real Time Chat App (Chatteric)",
    description: `A real time chat app allowing users to create 1:1 chat rooms with other users.  Implemented using React, Nest.js, and Socket.io, AWS Storage.  Deployed via Vercel and Render.  Was a personal project for learning websockets, Nest.js, and jwt authentication from scratch.  Uses AWS S3 storage for static assets (images) and OpenAPI Swagger for client generated apis + documentation.`,
    tools: ["react", "nest", "s3", "socket.io", "vercel", "render"],
    projectLink: "https://parsons-chat-app-client.vercel.app/",
    sourceCode: "https://gitlab.com/ericcparsons/chatteric",
    images: [
      "images/chatteric2.png",
      "images/chatteric1.png",
      "images/chatteric3.png",
    ],
    alt: "chatteric",
  },
  {
    id: 1,
    title: "Poll App",
    description:
      "A full-stack, basic poll application that allows users to vote on polls, as well as perform simple CRUD operatoins. ",
    tools: ["react", "mern", "express", "vercel", "render"],
    projectLink: "https://poll-mern-client.vercel.app",
    sourceCode: "https://gitlab.com/ericcparsons/poll-mern",
    images: ["images/polls1.png", "images/polls2.png", "images/polls3.png"],
    alt: "poll",
  },
  {
    id: 6,
    title: "Rock City Spin the Wheel",
    description:
      "A compact React app developed for Rock City Church’s “Spin the Wheel” sermon series. This app uses a random number generator to select a chapter from a randomly chosen book of the Bible. The book and chapter data are provided through a text file, which is updated by shell scripts executed via BitFocus Companion (StreamDeck).",
    tools: ["react", "tailwind", "vite", "bitfocus", "shell"],
    projectLink: "",
    sourceCode: "https://gitlab.com/ericcparsons/rc-spin-the-wheel",
    images: ["images/spin.gif"],
    alt: "poll",
  },
  {
    id: 2,
    title: "5 Day Weather Forecast App",
    description:
      "A weather forecast front-end project using React and the Redux Toolkit that connects to the OpenWeatherMap API.  A small, personal project back when I was learning React to improve skillset with Redux and connecting each to an API by utilizing Redux Thunks. Outdated but fun to remind how it all started. Deployed via Vercel.  ",
    tools: ["React", "Redux", "Node.js", "Vercel"],
    projectLink: "https://ericcparsons-weather.vercel.app",
    sourceCode: "https://gitlab.com/ericcparsons/weather-forecast",
    images: ["images/weather.png"],
    alt: "weather",
  },
  {
    id: 3,
    title: "COTS : Real Time Activity Status",
    description:
      "The Central Ohio Trauma System’s RTAS is a web-based platform for real-time ED patient acuity monitoring. As part of an OSU capstone project, the system was upgraded to include web and mobile interfaces, a REST API, and various data integrations. Although developer access was removed after deployment by COTS, the live site can still be viewed via the provided link, though authentication is no longer possible.",
    tools: ["react", "django"],
    projectLink: "https://www.cotsrtas.org/login",
    images: ["images/rtas1.png", "images/rtas2.png"],
    alt: "rtas",
  },
  {
    id: 4,
    title: "TCND Website",
    description:
      "Rails application developed for The Church Next Door in Columbus, OH. This platform features a comprehensive display of church resources and a blog with both editable and auto-updating posts. It also includes an admin panel with user authorization, allowing staff members to make updates. Please note that the application has since been transitioned to a WordPress site, so a live demo is no longer available.",
    tools: ["ruby", "rails", "postgres", "rails-admin"],
    projectLink: "",
    images: ["images/tcndhome.png"],
    alt: "tcnd",
  },
  {
    id: 5,
    title: "Personal Site",
    description: "The site you're looking at!",
    tools: ["react", "tailwind", "node.js", "vite", "vercel"],
    projectLink: "",
    sourceCode: "https://gitlab.com/ericcparsons/personal-site",
    images: ["images/personal-site.png"],
    alt: "tcnd",
  },
];
