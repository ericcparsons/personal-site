import { useEffect, useState } from "react";

const Navbar = () => {
  const [show, setShow] = useState(false);
  const [lastScrollY, setLastScrollY] = useState(0);

  const controlNavbar = () => {
    if (typeof window !== "undefined") {
      if (window.scrollY > 300) {
        // if scroll down show the navbar
        setShow(true);
      } else {
        // if scroll up show the navbar
        setShow(false);
      }

      // remember current page location to use in the next move
      setLastScrollY(window.scrollY);
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener("scroll", controlNavbar);
      return () => {
        window.removeEventListener("scroll", controlNavbar);
      };
    }
  }, [lastScrollY]);

  return (
    <div
      className="glass items-star fixed top-[-150px] z-30 flex w-full flex-col rounded-none px-4 py-5 shadow-md transition-all"
      style={show ? { top: "0px" } : {}}
      id="navbar"
    >
      <div className="font-bold">
        <h1 className="">
          <a className="text-ep-blue hover:no-underline" href="#home">
            eric parsons
          </a>
        </h1>
      </div>
      <ul className="text-grey flex gap-4 font-semibold text-ep-gray">
        <li>
          <a href="#about">about</a>
        </li>
        <li>
          <a href="#skills">skills</a>
        </li>
        <li>
          <a href="#projects">projects</a>
        </li>
      </ul>
    </div>
  );
};

export default Navbar;
