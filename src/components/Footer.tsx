import {
  IconBrandLinkedin,
  IconBrandGitlab,
  IconBrandGithub,
} from "@tabler/icons-react";
export const Footer = () => {
  return (
    <div className="topo flex flex-col items-center justify-center bg-main py-8 text-ep-white">
      <h2 className="text-4xl font-bold">eric parsons</h2>
      <h2 className="text-xl font-semibold">ecparsons42@gmail.com</h2>
      <div className="my-6 flex gap-8">
        <a href="https://www.linkedin.com/in/ericcparsons/">
          <IconBrandLinkedin size={48} />
        </a>
        <a href="https://gitlab.com/users/ericcparsons/projects">
          <IconBrandGitlab size={48} />
        </a>
        <a href="https://github.com/eparsons-updox">
          <IconBrandGithub size={48} />
        </a>
      </div>
      <h6>site by me &copy; twentytwentyfive</h6>
    </div>
  );
};
