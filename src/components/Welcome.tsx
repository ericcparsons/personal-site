import { Slide, Fade } from "react-awesome-reveal";

export const Welcome = () => {
  return (
    <div
      id="home"
      className="flex justify-start bg-ep-white font-sans font-bold text-main"
    >
      <div className="flex h-[100vh] flex-col justify-center px-[5%]">
        <Fade triggerOnce={true}>
          <Slide direction="up" triggerOnce={true}>
            <h1 className="text-5xl sm:text-6xl">hi, i'm eric. 👋🏼</h1>
          </Slide>
        </Fade>
        <Fade delay={250} triggerOnce={true}>
          <h2 className="text-2xl text-ep-blue">
            thanks for stopping by. i love to create and bring projects to life.
          </h2>
        </Fade>
      </div>
    </div>
  );
};
