import { Slide, Fade, Zoom } from "react-awesome-reveal";

export const About = () => {
  return (
    <div
      id="about"
      className="topo flex h-full items-center justify-center bg-main py-64 text-ep-white"
    >
      <div className="flex max-h-full max-w-[75%] flex-col items-center md:flex md:flex-row">
        <Zoom className="mr-2" triggerOnce={true}>
          <img className="mb-8 w-[50%] md:w-[88em]" src="images/me.png" />
        </Zoom>
        <div className="flex flex-col justify-center pl-5">
          <Slide direction="up" triggerOnce={true}>
            <h1 className="pb-5 text-5xl font-bold">
              full-stack software engineer
            </h1>
          </Slide>
          <Fade delay={250} triggerOnce={true}>
            <h5 id="skills" className="italic">
              From the outset of my career, I have been dedicated to the pursuit
              of knowledge and innovation, consistently guided by a profound
              appreciation for the technical field. I hold a Bachelor's degree
              in Computer and Information Science from The Ohio State University
              and am a Software Engineer with a strong focus on Web and Software
              Development. My expertise extends to Graphic Design, Photography,
              and Music Production. I place a high value on integrity,
              transparency, and meaningful professional relationships. Committed
              to excellence, I continuously seek to advance my skills and stay
              at the forefront of my field.
            </h5>
          </Fade>
        </div>
      </div>
    </div>
  );
};
