import { useState } from "react";
import { IProject, projects } from "../data/projects";
import { Project } from "./Project";
import { Slide, Fade } from "react-awesome-reveal";

export const Projects = () => {
  const [hoveringId, setHoveringId] = useState<number>();

  const updateHovered = (id: number) => setHoveringId(id);
  return (
    <div
      className="flex h-fit flex-col items-center justify-center bg-ep-white py-24 font-extrabold"
      id="projects"
    >
      <Slide direction="down" triggerOnce={true}>
        <h1 className="text-6xl">projects: </h1>
      </Slide>
      {projects.map((project: IProject, index: number) => {
        return (
          <div
            onClick={() => {
              if (project.projectLink || project.sourceCode) {
                window
                  .open(
                    project.projectLink
                      ? project.projectLink
                      : project.sourceCode,
                    "_blank",
                  )
                  ?.focus();
              }
            }}
            onMouseEnter={() => updateHovered(project.id)}
            onMouseLeave={() => updateHovered(-1)}
            className={`mt-5 flex w-full items-center justify-center sm:w-2/3 lg:justify-between xl:w-1/2 ${project.id === hoveringId ? "glass" : ""} ${project.projectLink || project.sourceCode ? "cursor-pointer" : "cursor-auto"}`}
          >
            <Fade triggerOnce={true} delay={250}>
              <Project project={project} index={index} />
            </Fade>
          </div>
        );
      })}
    </div>
  );
};
