import { IconExternalLink } from "@tabler/icons-react";
import { IProject } from "../data/projects";

interface ProjectProps {
  project: IProject;
  index: number;
}

export const Project = ({ project, index }: ProjectProps) => {
  return (
    <div className="mx-4 my-10 flex flex-col items-center justify-center gap-6 p-2 lg:flex-row">
      <img
        className="h-32 w-64 rounded-lg shadow-lg"
        src={project.images[0]}
        alt={`${project.alt}${index}`}
      />
      <div className="flex flex-col items-center justify-center lg:items-start">
        <div className="mb-4 flex items-center justify-center gap-2 lg:justify-start">
          {project.title}{" "}
          {(project.projectLink || project.sourceCode) && (
            <IconExternalLink size={18} />
          )}
        </div>
        <p className="text-center text-sm font-normal lg:text-start">
          {project.description}
        </p>
        <div className="mt-4 flex flex-wrap justify-center gap-4 whitespace-nowrap text-sm font-semibold lowercase">
          {project.tools?.map((tool) => {
            return (
              <div className="rounded-[16px] bg-ep-orange px-3 py-1 text-ep-white">
                {tool}
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
