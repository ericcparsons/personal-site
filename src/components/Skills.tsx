import { IconCode, IconSettings } from "@tabler/icons-react";
import { Slide, Fade } from "react-awesome-reveal";

export const Skills = () => {
  return (
    <div className="flex h-full justify-center bg-ep-white py-36">
      <div className="mt-[-300px] flex h-fit w-[75%] flex-col items-center justify-center rounded-[50px] bg-ep-white py-10 shadow-xl sm:w-[65%] md:w-[50%]">
        <div className="m-20 flex flex-col items-center">
          <Slide direction="up" fraction={1} triggerOnce={true}>
            <h1 className="mb-5 text-6xl font-extrabold">skills: </h1>
          </Slide>
          <div className="flex flex-col justify-between gap-24 sm:flex-row">
            <Fade triggerOnce={true} delay={250}>
              <div>
                <div className="mb-2 flex items-center gap-2 border-b-2 px-2 py-1 font-semibold">
                  <IconCode color="#F28A79" size={30} />
                  <span>languages </span>
                </div>
                <ul>
                  <li>typescript</li>
                  <li>javascript</li>
                  <li>node.js</li>
                  <li>kotlin</li>
                  <li>java</li>
                </ul>
              </div>
            </Fade>
            <Fade triggerOnce={true} delay={500}>
              <div>
                <div className="mb-2 flex items-center gap-2 border-b-2 px-2 py-1 font-semibold">
                  <IconSettings color="#F28A79" size={30} />
                  <span>development tools </span>
                </div>
                <ul>
                  <li>react</li>
                  <li>vue</li>
                  <li>tailwind</li>
                  <li>spring boot</li>
                  <li>docker</li>
                </ul>
              </div>
            </Fade>
          </div>
        </div>
      </div>
    </div>
  );
};
