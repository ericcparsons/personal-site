export const Contact = () => {
  return (
    <div className="flex justify-center bg-main py-12" id="contact">
      <div className="flex h-fit w-1/2 flex-col items-center justify-center rounded-[50px] bg-ep-white py-10 shadow-xl">
        <h1 className="text-6xl font-extrabold">contact:</h1>
        <h5 className="my-2 text-2xl">ecparsons42@gmail.com</h5>
        <div className="contact-content"></div>
      </div>
    </div>
  );
};
